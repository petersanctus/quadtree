# Quadtree Library

Python 🐍 library that implements a quadtree structure data. 

## Installation

Run the following to install:

```
pip install git+http://grou/dds/quadtree.git

ou

pip install git+http://10.1.0.6/dds/quadtree.git
```

### Usage:

```python
import quadtree as qt

# Create your own Point or inherit from qt
Class Point(qt.Point):
    #add attributes
    def __init__(self, x, y):
        super().__init__(x, y)
        
    # add properties functions etc.

capacity      = 4 # number of points each node can handle
qtree         = qt.QTree(qt.Rectangle(0, 0, 10, 10), capacity)
qrange        = qt.Rectangle(2, 2, 1, 1)
qcircle       = qt.Circle(3, 4, 2)

for i in range(0, 50): 
    x = random.randrange(0,10)
    y = random.randrange(0,10)
    qtree.insert(Point(x, y))

points = qtree.query_range(qrange)
points = qtree.query_circle(qcircle)
```

