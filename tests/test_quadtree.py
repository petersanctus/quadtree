import unittest
import random
from src import quadtree as qt
import math

class TPoint(qt.Point):
    def __init__(self, x, y, z=0):
        self._z = z
        super().__init__(x, y)

    @property
    def z(self) -> int:
        return self._z

class TestQuadtree(unittest.TestCase):


    def boundary_shift_stretch(self, boundary_in, shift_x, 
                                shift_y, shift_w, shift_h):
        x = boundary_in.x + shift_x
        y = boundary_in.y + shift_y
        h = boundary_in.height + shift_h
        w = boundary_in.width + shift_w
        return qt.Rectangle(x, y, w, h)  


    def shift_point(self, point: TPoint, x, y) -> TPoint:
        return TPoint(point.x + x, point.y + y)


    def setUp(self):
        self.rect_boundary1 = qt.Rectangle(200, 200, 200, 200)
        self.rect_boundary2 = self \
                             .boundary_shift_stretch(self \
                             .rect_boundary1, 0, 0, 200, 200) #200, 200, 400, 400
        self.rect_boundary3 = self \
                             .boundary_shift_stretch(self \
                             .rect_boundary2, -400, -400, 0, 0) #-200, -200, 400, 400
        self.boundary1 = [[0,400],[400,400],[400,0],[0,0] \
                         ,[0,400]]
        self.boundary2 = [[-200,600],[600,600],[600,-200],[-200,-200] \
                         ,[-200,600]]
        self.boundary3 = [[-600,200],[200,200],[200,-600],[-600,-600] \
                         ,[-600,200]]
        self.qt_teste4 = qt.QTree(self.rect_boundary1, 4)
        self.qt_teste10 = qt.QTree(self.rect_boundary1, 10)
        self.point1 = TPoint(100,300)
        self.point2 = TPoint(300,300)
        self.point3 = TPoint(300,100)
        self.point4 = TPoint(100,100)
        self.point5 = TPoint(150,360)
        self.point6 = TPoint(350,360)
        self.point7 = TPoint(350,160)
        self.point8 = TPoint(150,160)
        self.point9 = TPoint(5000,1600)
        self.circle_unit = qt.Circle(0,0,1)
        self.circle_2 = qt.Circle(0,0,2)
        self.circle_3 = qt.Circle(-1,0,2) 
        self.circle_4 = qt.Circle(0,-1,2) 
        self.circle_5 = qt.Circle(-1.5,-1.2,2) 

    def tearDown(self):
        pass


    def test_create_circle(self):
        self.assertIsInstance(self.circle_unit, qt.Circle)
        self.assertEqual(self.circle_unit.x , 0)
        self.assertEqual(self.circle_unit.y , 0)
        self.assertEqual(self.circle_unit.radius , 1)


    def test_circle_contains_point(self):
        self.assertTrue(self.circle_2.contains_point(TPoint(0,0)))
        self.assertTrue(self.circle_2.contains_point(TPoint(1,0)))
        self.assertTrue(self.circle_2.contains_point(TPoint(1,1)))
        self.assertTrue(self.circle_2.contains_point(TPoint(0,1)))
        self.assertTrue(self.circle_2.contains_point(TPoint(-1,1)))
        self.assertTrue(self.circle_2.contains_point(TPoint(-1,0)))
        self.assertTrue(self.circle_2.contains_point(TPoint(-1,-1)))
        self.assertTrue(self.circle_2.contains_point(TPoint(0,-1)))
        self.assertTrue(self.circle_2.contains_point(TPoint(1,-1)))
        self.assertTrue(self.circle_2.contains_point(TPoint(0,2)))
        self.assertTrue(self.circle_2.contains_point(TPoint(2,0)))
        self.assertTrue(self.circle_2.contains_point(TPoint(-2,0)))
        self.assertTrue(self.circle_2.contains_point(TPoint(0,-2)))


    def test_circle_contains_polygon(self):
        self.assertTrue(self
                       .circle_2.contains_polygon(
                        [[2,0],[0,2],[-2,0],[0,-2]]))
        self.assertTrue(not self
                       .circle_2.contains_polygon(
                        [[2,0],[0,2.1],[-2,0],[0,-2]]))

    
    def test_circle_dist_from_center_to_line(self):
        p1 = TPoint(0,1)
        p2 = TPoint(1,4)
        self.assertEqual(self.circle_2.dist_from_center_to_line(p1, p2) \
                       , math.sqrt(10)/10)
        self.assertEqual(self.circle_3.dist_from_center_to_line(p1, p2) \
                       , math.sqrt(10)/5)
        self.assertEqual(self.circle_4.dist_from_center_to_line(p1, p2) \
                       , math.sqrt(10)/5)
        self.assertEqual(round(self.circle_5.dist_from_center_to_line(p1, p2), 14) \
                       , round(23*math.sqrt(10)/100, 14))


    def test_circle_origin_inside_line_seg(self):
        p1 = TPoint(0,0)
        self.assertTrue(self \
                       .circle_2.origin_inside_line_seg(self \
                       .shift_point(p1, -1, -1), self \
                       .shift_point(p1, 1, -1)))
        self.assertTrue(self \
                       .circle_2.origin_inside_line_seg(self \
                       .shift_point(p1, -1, 1), self \
                       .shift_point(p1, 1, 1)))
        self.assertTrue(self \
                       .circle_2.origin_inside_line_seg(self \
                       .shift_point(p1, -1, 1), self \
                       .shift_point(p1, 1, 1)))
        self.assertTrue(self \
                       .circle_2.origin_inside_line_seg(self \
                       .shift_point(p1, -1, 1), self \
                       .shift_point(p1, 1, 1)))
        self.assertTrue(self \
                       .circle_2.origin_inside_line_seg(self \
                       .shift_point(p1, -1, 1), self \
                       .shift_point(p1, 1, 1)))


    def test_create_rectangle(self):        
        self.assertListEqual(self \
                            .rect_boundary1.boundary_polygon, self.boundary1)
        self.assertListEqual(self \
                            .rect_boundary2.boundary_polygon, self.boundary2)
        self.assertListEqual(self \
                            .rect_boundary3.boundary_polygon, self.boundary3)


    def test_rectangle_contains_point(self):
        self.assertTrue(self.rect_boundary1.contain_point(TPoint(10,200)))
        self.assertTrue(self.rect_boundary1.contain_point(TPoint(10,200)))
        self.assertTrue(self.rect_boundary1.contain_point(TPoint(400,400)))
        self.assertTrue(self.rect_boundary1.contain_point(TPoint(10,200)))
        self.assertTrue(self.rect_boundary1.contain_point(TPoint(200,200)))
        self.assertTrue(self.rect_boundary1.contain_point(TPoint(200,20)))
        self.assertTrue(self.rect_boundary1.contain_point(TPoint(0,0)))
        self.assertTrue(self.rect_boundary1.contain_point(TPoint(100,0)))
        self.assertTrue(not self.rect_boundary1.contain_point(TPoint(-100,0)))
        self.assertTrue(not self.rect_boundary1.contain_point(TPoint(100,-10)))


    def test_rectangle_contains_rect_boundary(self):
        boundary_test = self.boundary_shift_stretch(
                        self.rect_boundary1, 0, 0, -1, -1)
        self.assertTrue(self.rect_boundary1.contain_rect_boundary(boundary_test))
        boundary_test = self.boundary_shift_stretch(
                        self.rect_boundary1, 0, 0, 0, 0)
        self.assertTrue(self.rect_boundary1.contain_rect_boundary(boundary_test))
        boundary_test = self.boundary_shift_stretch(
                        self.rect_boundary1, 0, 0, 1, 1)
        self.assertTrue(not self.rect_boundary1.contain_rect_boundary(boundary_test))
    

    def test_rectangle_intersects_rectangle(self):
        boundary_test = self \
                        .boundary_shift_stretch(self \
                        .rect_boundary1, 0, 0, -1, -1)
        self.assertTrue(self.rect_boundary1.intersects_rectangle(boundary_test))
        boundary_test = self \
                       .boundary_shift_stretch(self \
                       .rect_boundary1, 1, 1, 0, 0)
        self.assertTrue(self.rect_boundary1.intersects_rectangle(boundary_test))
        boundary_test = self \
                       .boundary_shift_stretch(self \
                       .rect_boundary1, 400, 0, 0, 0)
        self.assertTrue(self.rect_boundary1.intersects_rectangle(boundary_test))
        boundary_test = self \
                       .boundary_shift_stretch(self \
                       .rect_boundary1, 0, 400, 0, 0)
        self.assertTrue(self.rect_boundary1.intersects_rectangle(boundary_test))
        boundary_test = self \
                       .boundary_shift_stretch(self
                       .rect_boundary1, 401, 0, 0, 0)
        self.assertTrue(not self \
                       .rect_boundary1.intersects_rectangle(boundary_test))
        boundary_test = self \
                       .boundary_shift_stretch(self
                       .rect_boundary1, 0, 401, 0, 0)
        self.assertTrue(not self \
                       .rect_boundary1.intersects_rectangle(boundary_test))
        boundary_test = self \
                       .boundary_shift_stretch(self \
                       .rect_boundary1, -300, -300, 0, 0)
        rect_boundary1 = self \
                       .boundary_shift_stretch(self \
                       .rect_boundary1, -100, -100, 0, 0)
        self.assertTrue(self.rect_boundary1.intersects_rectangle(boundary_test))


    def test_rectangle_intersects_circle_boundary(self):
        rect_boundary_to_test = qt.Rectangle(0, 0, 3, 3)
        self.assertTrue(rect_boundary_to_test.intersects_circle(self.circle_2))
        rect_boundary_to_test = qt.Rectangle(0, 0, 2, 2)
        self.assertTrue(rect_boundary_to_test.intersects_circle(self.circle_2))
        rect_boundary_to_test = qt.Rectangle(0, 0, 0.5, 0.5)
        self.assertTrue(rect_boundary_to_test.intersects_circle(self.circle_2))
        rect_boundary_to_test = qt.Rectangle(2, 0, 0.5, 0.5)
        self.assertTrue(rect_boundary_to_test.intersects_circle(self.circle_2))
        rect_boundary_to_test = qt.Rectangle(-2, 0, 0.5, 0.5)
        self.assertTrue(rect_boundary_to_test.intersects_circle(self.circle_2))
        rect_boundary_to_test = qt.Rectangle(2.501, 0, 0.5, 0.5)
        self.assertTrue(not \
                        rect_boundary_to_test \
                       .intersects_circle(self.circle_2))
        rect_boundary_to_test = qt.Rectangle(-2.501, 0, 0.5, 0.5)
        self.assertTrue(not \
                        rect_boundary_to_test \
                       .intersects_circle(self.circle_2))
        

    def test_create_qtree(self):
        qt_teste_empty = qt.QTree(self.rect_boundary1, 4)
        self.assertIsNotNone(qt_teste_empty.boundary)
        self.assertIsNotNone(qt_teste_empty.divided)
        self.assertIsNotNone(qt_teste_empty.points)
        self.assertNotEqual(qt_teste_empty.depth, 0)
        self.assertNotEqual(qt_teste_empty.capacity, 0)


    def test_qtree_boundary_contains_point(self):
        self.assertTrue(self.qt_teste4.boundary_contain_point(TPoint(10,200)))
        self.assertTrue(self.qt_teste4.boundary_contain_point(TPoint(10,200)))
        self.assertTrue(self.qt_teste4.boundary_contain_point(TPoint(200,200)))
        self.assertTrue(self.qt_teste4.boundary_contain_point(TPoint(200,20)))
        self.assertTrue(self.qt_teste4.boundary_contain_point(TPoint(0,0)))
        self.assertTrue(self.qt_teste4.boundary_contain_point(TPoint(100,0)))
        self.assertTrue(not self.qt_teste4.boundary_contain_point(TPoint(-100,0)))
        self.assertTrue(not self.qt_teste4.boundary_contain_point(TPoint(100,-10)))


    def test_qtree_insert_point(self):
        self.qt_teste4.insert(self.point9)
        self.assertTrue(len(self.qt_teste4.points) == 0)

        self.qt_teste4.insert(self.point1)
        self.assertTrue(TPoint(100,300) in self.qt_teste4.points)        
        self.qt_teste4.insert(self.point2)
        self.assertTrue(TPoint(300,300) in self.qt_teste4.points)
        self.qt_teste4.insert(self.point3)
        self.assertTrue(TPoint(300,100) in self.qt_teste4.points)
        self.qt_teste4.insert(self.point4)
        self.assertTrue(TPoint(100,100) in self.qt_teste4.points)

        self.qt_teste4.insert(self.point5)
        self.assertTrue(not self.point5 in self.qt_teste4.points)
        self.assertTrue(not self.point5 in self.qt_teste4.northeast.points)
        self.assertTrue(not self.point5 in self.qt_teste4.southeast.points)
        self.assertTrue(not self.point5 in self.qt_teste4.southwest.points)
        self.assertTrue(self.point5 in self.qt_teste4.northwest.points)

        self.qt_teste4.insert(self.point6)
        self.assertTrue(not self.point6 in self.qt_teste4.points)
        self.assertTrue(not self.point6 in self.qt_teste4.northwest.points)
        self.assertTrue(not self.point6 in self.qt_teste4.southeast.points)
        self.assertTrue(not self.point6 in self.qt_teste4.southwest.points)
        self.assertTrue(self.point6 in self.qt_teste4.northeast.points)

        self.qt_teste4.insert(self.point7)
        self.assertTrue(not self.point7 in self.qt_teste4.points)
        self.assertTrue(not self.point7 in self.qt_teste4.northeast.points)
        self.assertTrue(not self.point7 in self.qt_teste4.northwest.points)
        self.assertTrue(not self.point7 in self.qt_teste4.southwest.points)
        self.assertTrue(self.point7 in self.qt_teste4.southeast.points)

        self.qt_teste4.insert(self.point8)
        self.assertTrue(not self.point8 in self.qt_teste4.points)
        self.assertTrue(not self.point8 in self.qt_teste4.northeast.points)
        self.assertTrue(not self.point8 in self.qt_teste4.southeast.points)
        self.assertTrue(not self.point8 in self.qt_teste4.northwest.points)
        self.assertTrue(self.point8 in self.qt_teste4.southwest.points)

        #self.qt_teste4.graph(2)

    def test_qtree_subdivide_qtree(self):
        self.assertTrue(not self.qt_teste4.divided)
        self.qt_teste4._subdivide()
        self.assertTrue(self.qt_teste4.divided)
        self.assertListEqual(                                       \
              self.qt_teste4.boundary.boundary_polygon              \
            , [[0,400],[400,400],[400,0],[0,0],[0,400]])
        self.assertListEqual(                                       \
              self.qt_teste4.northwest.boundary.boundary_polygon    \
            , [[0,400],[200,400],[200,200],[0,200],[0,400]])
        self.assertListEqual(                                       \
              self.qt_teste4.northeast.boundary.boundary_polygon    \
            , [[200,400],[400,400],[400,200],[200,200],[200,400]])
        self.assertListEqual(                                       \
              self.qt_teste4.southeast.boundary.boundary_polygon    \
            , [[200,200],[400,200],[400,0],[200,0],[200,200]])
        self.assertListEqual(                                       \
              self.qt_teste4.southwest.boundary.boundary_polygon    \
            , [[0,200],[200,200],[200,0],[0,0],[0,200]])


    def test_qtree_max_depth(self):
        self.assertEqual(self.qt_teste4.max_depth(), 1)
        self.qt_teste4._subdivide()
        self.assertEqual(self.qt_teste4.max_depth(), 2)
        self.qt_teste4.northwest._subdivide()
        self.assertEqual(self.qt_teste4.max_depth(), 3)
        self.qt_teste4.northwest.northwest._subdivide()
        self.assertEqual(self.qt_teste4.max_depth(), 4)


    def test_qtree_big_random_insert(self): 
        qt_teste_in = self.qt_teste4
        for i in range(0, 50): 
            x = random.randrange(0,399)
            y = random.randrange(0,399)
            z = random.randrange(0,100)
            qt_teste_in.insert(TPoint(x, y, z))
        for i in range(0, 50): 
            x = random.randrange(1,200)
            y = random.randrange(1,200)
            z = random.randrange(0,100)
            qt_teste_in.insert(TPoint(x, y, z))
        for i in range(0, 50): 
            x = random.randrange(100,200)
            y = random.randrange(100,200)
            z = random.randrange(0,100)
            qt_teste_in.insert(TPoint(x, y, z))
        for i in range(0, 50): 
            x = random.randrange(100,150)
            y = random.randrange(100,150)
            z = random.randrange(0,100)
            qt_teste_in.insert(TPoint(x, y, z))
        print("\nMax depth: {}\n".format(qt_teste_in.max_depth()))
        self.assertEqual(len(qt_teste_in.all_points()), 200)           
        # qt_teste_in.graph(2)


    def test_qtree_query_rectangle(self):
        qt_teste_in = self.qt_teste4
        range_to_query = qt.Rectangle(100,100,100,100)
        for i in range(0, 4):
            x = random.randrange(1,100)
            y = random.randrange(1,100)
            qt_teste_in.insert(TPoint(x, y))
        qt_teste_in.insert(TPoint(300, 300))
        qt_teste_in.insert(TPoint(10, 200))
        qt_teste_in.insert(TPoint(200, 10))
        qt_teste_in.insert(TPoint(0, 10))
        qt_teste_in.insert(TPoint(10, 0))
        qt_teste_in.insert(TPoint(20, 10))
        qt_teste_in.insert(TPoint(21, 10))
        qt_teste_in.insert(TPoint(22, 10))
        self.assertEqual(qt_teste_in.max_depth(), 3)
        self.assertEqual(len(qt_teste_in.query_range(range_to_query)), 11)


    def test_qtree_query_circle(self):
        qt_teste_in = self.qt_teste4
        circle_to_query = qt.Circle(50, 50, 70.711)        
        qt_teste_in.insert(TPoint(300, 300)) # q2 d1
        qt_teste_in.insert(TPoint(10, 200))  # q1 d1
        qt_teste_in.insert(TPoint(200, 300))  # q3 d1
        qt_teste_in.insert(TPoint(190, 190)) # q4 d1
        qt_teste_in.insert(TPoint(110, 50))  # q4 d2 o
        qt_teste_in.insert(TPoint(20, 10))   # q4 d2 o
        qt_teste_in.insert(TPoint(21, 20))   # q4 d2 o
        qt_teste_in.insert(TPoint(22, 30))   # q4 d2 o
        qt_teste_in.insert(TPoint(99, 80))   # q4 d3 o
        qt_teste_in.insert(TPoint(90, 85))   # q4 d3 o
        qt_teste_in.insert(TPoint(22, 60))   # q4 d3 o
        qt_teste_in.insert(TPoint(12, 30))   # q4 d3 o
        qt_teste_in.insert(TPoint(30, 45))   # q4 d4 o
        self.assertEqual(qt_teste_in.max_depth(), 4)
        self.assertEqual(len(qt_teste_in.query_circle(circle_to_query)), 9)
        # qt_teste_in.graph(2)

if __name__ == '__main__':
    unittest.main()

