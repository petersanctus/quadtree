# import quadtree as qt
from src import quadtree as qt
import laspy
import numpy as np
import time
import math

class LasPoint_3():
    """
    Format 3:
    "X": "i4",
    "Y": "i4",
    "Z": "i4",
    "intensity": "u2",
    "bit_fields": "u1",
    "raw_classification": "u1",
    "scan_angle_rank": "i1",
    "user_data": "u1",
    "point_source_id": "u2",
    "gps_time": "f8",
    "red": "u2",
    "green": "u2",
    "blue": "u2"   
    """

    # sub fields of the 'bit_fields' dimension
    RETURN_NUMBER_MASK_0 = 0b00000111
    NUMBER_OF_RETURNS_MASK_0 = 0b00111000
    SCAN_DIRECTION_FLAG_MASK_0 = 0b01000000
    EDGE_OF_FLIGHT_LINE_MASK_0 = 0b10000000

    # sub fields of the 'raw_classification' dimension
    CLASSIFICATION_MASK_0 = 0b00011111
    SYNTHETIC_MASK_0 = 0b00100000
    KEY_POINT_MASK_0 = 0b01000000
    WITHHELD_MASK_0 = 0b10000000   

    def __init__(self, np_line, scale):        
        self._x = np_line['X']*scale[0]
        self._y = np_line['Y']*scale[1]
        self._z = np_line['Z']*scale[2]
        self._scale = scale
        self._gpstime = np_line['gps_time']
        self._raw_classification = np_line['raw_classification']        
        
    @property
    def x(self) -> int:
        return self._x
    
    @property
    def y(self) -> int:
        return self._y

    @property
    def z(self) -> int:
        return self._z

    @property
    def intensity(self) -> int:
        return self._intensity

    @property
    def gpstime(self) -> int:
        return self._gpstime

    @property
    def bit_fields(self):
        return self._bit_fields

    @property
    def return_number(self) -> int:
        return LasPoint_3.RETURN_NUMBER_MASK_0 & self._bit_fields

    @property
    def number_of_returns(self) -> int:
        return LasPoint_3.NUMBER_OF_RETURNS_MASK_0 & self._bit_fields

    @property
    def classification(self) -> int:
        return LasPoint_3.CLASSIFICATION_MASK_0 & self._raw_classification

    @property
    def raw_classification(self) -> int:
        return self._raw_classification

    @property
    def red(self) -> int:
        return self._red   

    @property
    def green(self) -> int:
        return self._green    

    @property
    def blue(self) -> int:
        return self._blue

    @property
    def scale(self) -> int:
        return self._scale

    @property
    def id(self) -> int:
        return self._id

    @raw_classification.setter
    def raw_classification(self, value: int):
        self._raw_classification = value

    @gpstime.setter
    def gpstime(self, value: float):
        self._gpstime = value

    @bit_fields.setter
    def bit_fields(self, value: int):
        _bit_fields = value

    @red.setter    
    def red(self, value: int):
        self._red= value

    @green.setter
    def green(self, value: int):
        self._green= value    

    @blue.setter
    def blue(self, value: int):
        self._blue = value

    @scale.setter
    def scale(self, value: float):
        self._scale = value

    def higher_then(self, other) -> bool:
        return self.z > other.z
    
    def __gt__(self, other) -> bool:
        return self.z > other.z

    def point_to_array(self):
        return [self.x, self.y, self.z, self.classification, self.gpstime]

    def __str__(self):
        return f'{self.x} {self.y} {self.z} {self.classification} {self.gpstime}'


#### Import data -------------------------------------------------------------------------
work_folder = r"G:\tmp\Data Analysis\E-redes"
start_time = time.time()
las_file   = laspy.read(work_folder + r"\amostra_total.las")
end_time   = time.time()

print(f'Time to import las: {start_time - end_time}\n')


width    = (las_file.header.x_max - las_file.header.x_min)/2
height   = (las_file.header.y_max - las_file.header.y_min)/2
x        = las_file.header.x_min + width
y        = las_file.header.y_min + height
capacity = 100 # number of points each node can handle
qtree    = qt.QTree(qt.Rectangle(x, y, width, height), capacity)

point_format = las_file.point_format
dimensions   = list(point_format.dimension_names)


#### Test Query All Points ---------------------------------------------------------------
""" def check_in_buffer(x, y, circle: qt.Circle) -> bool:
        x_dif = x - circle.x
        y_dif = y - circle.y
        return math.hypot(x_dif, y_dif) <= circle.radius


qrange     = qt.Circle(562674.48, 4479548.9, 3)
start_time = time.time()
in_buffer  = []

for line in las_file.points[las_file.classification == 16].array:
    if check_in_buffer(line[0]/100, line[1]/100, qrange):
        in_buffer += [[line[0]/100, line[1]/100]] 
end_time = time.time()
print(f'Time to query all data: {start_time - end_time}')
print(f'{len(in_buffer)} point found.\n') """

#### Check Time to Fill  -----------------------------------------------------------------
start_time = time.time()
""" classes_of_interest = np.logical_or(las_file.classification == 2,
                                    las_file.classification == 3,
                                    las_file.classification == 4,
                                    las_file.classification == 5,
                                    las_file.classification == 12,
                                    las_file.classification == 16) """

for line in las_file.points[las_file.classification == 16].array:
    pt = LasPoint_3(line, las_file.header.scales)
    qtree.insert(pt)

end_time = time.time()
print(f'Quadtree depth: {qtree.max_depth()}\n')
print(f'Time to fill qtree: {start_time - end_time}\n')


#### Check Time to Query With Rectangle  -------------------------------------------------
""" start_time = time.time()
qrange     = qt.Rectangle(562841.20, 4479509.01, 3, 3)
points     = qtree.query_range(qrange)
end_time   = time.time()
print(f'Time to query qtree: {start_time - end_time}\n')

with open(work_folder + r"\test_boundary_rect.txt",'w') as f_out:
    for point in points:
        f_out.write(str(point))
        f_out.write("\n") """

#### Check Time to Query With Circle  ----------------------------------------------------
""" start_time = time.time()
qrange     = qt.Circle(562841.20, 4479509.01, 3)
points     = qtree.query_circle(qrange)
end_time   = time.time()
print(f'Time to query qtree: {start_time - end_time}\n')

with open(work_folder + r"\test_boundary_circle.txt",'w') as f_out:
    for point in points:
        f_out.write(str(point))
        f_out.write("\n") """

size_x = 18
size_y = round(qtree.boundary.height / qtree.boundary.width * size_x)
qtree.graph(1, size_x, size_y)

""" new_las_file = laspy.create(
            point_format=las_file.header._point_format, 
            file_version=las_file.header.version)
new_las_file.points = las_file.points[las_file.classification != 7]
new_las_file.write(r'J:\Trabalho\Albatroz\Projetos\E-Redes\Out_Las\amostra_zona_norte_sem_7.las') """
