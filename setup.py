from setuptools import setup

version = '0.1.4'


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="quadtree_abz",
    version=version,
    description="Python library that implements a quadtree structure data.",
    py_modules=["quadtree"],
    package_dir={"": "src"},
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.9",
        "License Copyright :: (c) 2021 Albatroz Engenharia",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Mathematics",
        "Topic :: Scientific/Engineering :: GIS",
        "Topic :: Software Development :: Libraries",
    ],
    url="http://10.1.0.6/dds/quadtree",
    include_package_data=True,
    install_requires=[
       "matplotlib >= 3.4.1",
    ],
    author="Pedro Mendonça",
    author_email="pedro.mendonca@albatroz-eng.com",
)
