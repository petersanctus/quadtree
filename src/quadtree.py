import matplotlib.pyplot as pyplt
import matplotlib.patches as pltpatches
import math

class Point:
    def __init__(self, x, y):
        self._x = x
        self._y = y
    
    @property
    def x(self):
        return self._x
    
    @property               
    def y(self):
        return self._y
        
    def __str__(self):
        return "x: {0} ".format(self._x) + "y: {0}".format(self._y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

class Circle:

    def __init__(self, x, y, radius):
        self._x = x
        self._y = y
        self._radius = radius

    @property
    def x(self):
        return self._x
    
    @property
    def y(self):
        return self._y

    @property
    def radius(self):
        return self._radius
        

    def contains_point(self, point) -> bool:
        x_dif = point.x - self.x
        y_dif = point.y - self.y
        return math.hypot(x_dif, y_dif) <= self._radius


    def contains_polygon(self, polygon) -> bool:
        for point in polygon:
            if not self.contains_point(Point(point[0], point[1])):
                return False
        return True

    # https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    def dist_from_center_to_line(self, p1, p2) -> float:
        return abs (                                      
                      ((p2.x - p1.x) * (p1.y - self.y))   
                    - ((p1.x - self.x) * (p2.y - p1.y))   
                   ) / math.sqrt(                                      
                               math.pow((p2.x - p1.x), 2)          
                             + math.pow((p2.y - p1.y), 2)          
                             )              
    
    def origin_inside_line_seg(self, p1, p2) -> bool:
        return self.x > p1.x and self.x < p2.x \
            or self.x > p2.x and self.x < p1.x \
            or self.y > p1.y and self.y < p2.y \
            or self.y > p2.y and self.y < p1.y

class Rectangle: 
    """
    A rectangle defined via an anchor point *xy* and its *width* and *height*.

    The rectangle extends from Ox + width in positive and negative x-direction
    and from Oy + height in positive and negative y-direction.

      :                +------------------+------------------+
      :                |                  |                  |
      :                |                height               |
      :                |                  |                  |
      :                |                 (xy)---- width -----+
      :                |                                     |
      :                |                                     |
      :                |                                     |
      :                +------------------+------------------+
    
    """   
    
    def __init__(self, x, y, w, h):        
        self._x = x
        self._y = y
        self._width = w
        self._height = h

    @property
    def x(self):
        return self._x
    
    
    @property               
    def y(self):
        return self._y
        
        
    @property
    def width(self):
        return self._width
    
    
    @property               
    def height(self):
        return self._height

    @property
    def boundary_polygon(self) -> list:
        return [  [self.x - self.width, self.y + self.height] ,
                  [self.x + self.width, self.y + self.height] ,
                  [self.x + self.width, self.y - self.height] ,
                  [self.x - self.width, self.y - self.height] ,
                  [self.x - self.width, self.y + self.height] ]

    
    def contain_point(self, point) -> bool:        
        return point.x <= self.x + self.width  \
          and point.x >= self.x - self.width   \
          and point.y <= self.y + self.height  \
          and point.y >= self.y - self.height

    
    def contain_rect_boundary(self, rectangle) -> bool: 
        return rectangle.x - rectangle.width >= self.x - self.width and  \
               rectangle.x + rectangle.width <= self.x + self.width and  \
               rectangle.y - rectangle.width >= self.y - self.height and \
               rectangle.y + rectangle.width <= self.x + self.height


    def intersects_rectangle(self, rectangle) -> bool:
        return not (rectangle.x - rectangle.width > self.x + self.width or   \
                    rectangle.x + rectangle.width < self.x - self.width or   \
                    rectangle.y - rectangle.height > self.y + self.height or \
                    rectangle.y + rectangle.height < self.y - self.height)

    
    def intersects_circle(self, circle: Circle) -> bool:
        polygon = self.boundary_polygon
        for p1, p2 in zip(polygon[::1], polygon[1::1]):
            p1 = Point(p1[0], p1[1])
            p2 = Point(p2[0], p2[1])
            if self.contain_point(Point(circle.x, circle.y)):
                return True            
            if circle.contains_point(p1) or circle.contains_point(p2):
                return True
            if circle.dist_from_center_to_line(p1, p2) <= circle.radius:
                if circle.origin_inside_line_seg(p1, p2):
                    return True
        return False
    

    def __str__(self):
        return "x: {0} ".format(self.x)               \
             + "y: {0} ".format(self.y)               \
             + "h: {0} ".format(self.height)          \
             + "w: {0} ".format(self.width)           \
             + "\nPolygon: \n"                        \
             + "["  + str(self.x - self.width) + ", " \
             + str(self.y + self.height)              \
             + "\n" + str(self.x + self.width) + ", " \
             + str(self.y + self.height)              \
             + "\n" + str(self.x + self.width) + ", " \
             + str(self.y - self.height)              \
             + "\n" + str(self.x - self.width) + ", " \
             + str(self.y - self.height) + "]"

class QTree:    
    
    def __init__(self, boundary: Rectangle, capacity=0, depth=0):
        self._boundary = boundary
        self._capacity = capacity
        self._devided = False
        self._points = []
        self._depth = depth + 1 

    @property
    def capacity(self):
        return self._capacity

    @property
    def depth(self):
        return self._depth

    @property
    def points(self):
        return self._points

    @property
    def boundary(self):
        return self._boundary

    @property
    def northwest(self):
        return self._northwest

    @property
    def northeast(self):
        return self._northeast

    @property
    def southeast(self):
        return self._southeast

    @property
    def southwest(self):
        return self._southwest
    
    @property
    def divided(self):
        return self._devided

    @property
    def sub_boundaries(self):
        return [self._northwest, self._northeast, \
                self._southeast, self._southwest]

    #This method is diferent from rectengle.contain_point. It must be used for insert pt
    def boundary_contain_point(self, point) -> bool:        
        return point.x < self._boundary.x + self._boundary.width  \
          and point.x >= self._boundary.x - self._boundary.width  \
          and point.y < self._boundary.y + self._boundary.height  \
          and point.y >= self._boundary.y - self._boundary.height


    def _subdivide(self):        
        self_x = self._boundary.x
        self_y = self._boundary.y
        self_w = self._boundary.width
        self_h = self._boundary.height
        nw = Rectangle(self_x - self_w / 2, self_y + self_h / 2, 
                       self_w / 2, self_h / 2)
        ne = Rectangle(self_x + self_w / 2, self_y + self_h / 2, 
                       self_w / 2, self_h / 2)
        se = Rectangle(self_x + self_w / 2, self_y - self_h / 2, 
                       self_w / 2, self_h / 2)
        sw = Rectangle(self_x - self_w / 2, self_y - self_h / 2, 
                       self_w / 2, self_h / 2)                
        self._northwest = QTree(nw, self._capacity, self._depth)
        self._northeast = QTree(ne, self._capacity, self._depth)
        self._southeast = QTree(se, self._capacity, self._depth)
        self._southwest = QTree(sw, self._capacity, self._depth)    
        self._devided = True
        
        
    def insert(self, point):
        """Inserts a point in the quadtree
        
        Parameters
        ----------
        point : 
            point to insert in the quadtree.
        """     
        if not self.boundary_contain_point(point):
            return
        if len(self._points) >= self._capacity:
            if not self._devided:
                self._subdivide()
            self._northwest.insert(point)
            self._northeast.insert(point)
            self._southeast.insert(point)
            self._southwest.insert(point)
            return
        self._points.append(point)

    
    def find_all_rectangles(self) -> list:
        """
        Returns
        -------
        list
            a list with all the boundaries found in the quadtree including the root.
        """
        if not self._devided:
            return [self._boundary]
        boundaries = []
        for qtree in self.sub_boundaries:
            boundaries += (qtree.find_all_rectangles())
        return boundaries


    def all_points(self) -> list:
        """
        Returns
        -------
        list
            a list with all the points found in the quadtree.
        """
        if not self._devided:
            return self._points
        list_of_all_points = []
        list_of_all_points += self._points
        for qtree in self.sub_boundaries:
            list_of_all_points += (qtree.all_points())
        return list_of_all_points


    def max_depth(self) -> int:
        """
        Depth value is the number of steps below the root of the quadtree, incrementing from
        1 (the root) till the deepest child. 

        Returns
        -------
        int
            value of the depth of the deepest child of the quadtree.
        """
        if not self._devided:
            return self.depth
        qt_total_depth = self.depth        
        for qtree in self.sub_boundaries:
            a = qtree.max_depth()
            qt_total_depth = qt_total_depth \
                          if qt_total_depth > qtree.max_depth() \
                        else qtree.max_depth()
        return qt_total_depth


    def query_range(self, range: Rectangle) -> list:
        """Querying the quadtree with a rectangle format buffer to search for points
        
        NOTE: the rectangle has the same orientation as the bounderies of the QTree
        
        Parameters
        ----------
        range : qt.Rectangle
            A range/boundary to where qtree will search points.

        Returns
        -------
        list
            a list with all the points found inside circle area.
        """
        points_in_range = []        
        if not self.boundary.intersects_rectangle(range):
            return []
        if range.contain_rect_boundary(self.boundary):
            points_in_range += self.all_points()
            return points_in_range
        for point in self.points:
            if range.contain_point(point):
                points_in_range.append(point)
        if self._devided:
            for qtree in self.sub_boundaries:
                points_in_range += qtree.query_range(range)
        return points_in_range


    def query_circle(self, circle_to_query: Circle) -> list:
        """Querying the quadtree with a circle format buffer to search for points
        
        Parameters
        ----------
        circle_to_query : qt.Circle
            A Circle to where qtree will search points.

        Returns
        -------
        list
            a list with all the points found inside circle area.
        """
        points_in_circle = []
        if not self.boundary.intersects_circle(circle_to_query):
            return []
        if circle_to_query.contains_polygon(self.boundary.boundary_polygon):
            points_in_circle += self.all_points()
            return points_in_circle
        for point in self.points:
            if circle_to_query.contains_point(point):
                points_in_circle.append(point)
        if self._devided:
            for qtree in self.sub_boundaries:
                points_in_circle += qtree.query_circle(circle_to_query)
        return points_in_circle


    def plot_qtree(self, point_size: float, plot_size_x=13, plot_size_y=6, alphaColor=1.0, colormap='jet'):
        fig = pyplt.figure(figsize=(plot_size_x, plot_size_y))
        pyplt.title("QTree")
        ax = fig.add_subplot(111)
        ax.axis("off")
        c = self.find_all_rectangles()
        print("Number of segments: %d" %len(c))
        areas = set()
        for el in c:
            areas.add(el.width*2*el.height*2*100)
        print("Minimum segment area: %.3f units" %min(areas)) 
        for n in c:
            ax.add_patch(
                      pltpatches.Rectangle((n.x - n.width, n.y - n.height) 
                                          , n.width * 2                                          
                                          , n.height * 2                                     
                                          , fill=False
                                          , linewidth=.1))
        
        
        all_points_in_qtree = self.all_points()
        if isinstance(getattr(type(self.points[0]), 'z', None), property):
            all_points_in_qtree = sorted(all_points_in_qtree, key=lambda point: point.z)
            x = [point.x for point in all_points_in_qtree]
            y = [point.y for point in all_points_in_qtree]  
            z = [point.z for point in all_points_in_qtree]
            pyplt.scatter(x, y, s=point_size, c=z, marker='.', alpha=None, cmap=colormap)
        else:
            x = [point.x for point in all_points_in_qtree]
            y = [point.y for point in all_points_in_qtree]
            pyplt.scatter(x, y, s=point_size, c='#ff0000d0', marker='.', alpha=alphaColor)
        pyplt.show()
        return


    def print_points(self):
        stringPoints = ""
        for i in self._boundary.points: 
            stringPoints += "\n" + str(i)
        return stringPoints

        
    def __str__(self):
        if not self._devided:
            return "\nDepth=" + str(self._depth) + " boundary: " \
                    + str(self._boundary) \
                    + "\nPoints: {0}\n".format(str(self._boundary \
                                                  .point_count_in_node))
        
        return "\nDepth=" + str(self._depth) + " boundary: " \
                + str(self._boundary) \
                + "\nPoints: {0}\n".format(str(self._boundary \
                                               .point_count_in_node)) \
                + "\nnw: " + self._northwest.__str__() \
                + "\nne: " + self._northeast.__str__() \
                + "\nse: " + self._southeast.__str__() \
                + "\nsw: " + self._southwest.__str__()